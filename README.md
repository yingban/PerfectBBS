# Springboot动漫论坛项目

## 介绍
此项目可以对帖子进行增删改查和查看发布的帖子详情【目前仅做为2021年7月份毕设使用】

## 软件架构
后端：SpringBoot,JPA,Thymeleaf模板
数据库:mysql
前端:Semantic UI框架


## 资料地址
---------------------------------------------------------------
#### 1. Semantic UI官网](https://semantic-ui.com/)
#### 2. JS(https://www.jsdelivr.com/)
#### 3. 编辑器 Markdown(https://pandao.github.io/editor.md/)
#### 4. 内容排版 typo.css(https://github.com/sofish/typo.css)
#### 5. 目录生成 Tocbot(https://tscanlin.github.io/tocbot/)
#### 6. 图片地址 (https://www.photock.jp/detail/area/1337/)
#### 7. 图片储存地址 (https://imgtu.com/)
---------------------------------------------------------------

## 下面是一些效果图展示
##### 首页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnFitI.png "在这里输入图片标题")
##### 分类页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnAUYR.png "在这里输入图片标题")


##### 标签页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnANk9.png "在这里输入图片标题")

##### 全部页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnAJw4.png "在这里输入图片标题")

##### 登录页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnVHWn.png "在这里输入图片标题")

##### 登录成功页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnVxwF.png "在这里输入图片标题")

##### 评论页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnAGmF.png "在这里输入图片标题")

##### 关于本站页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnVgzt.png "在这里输入图片标题")

##### 帖子管理页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnZGm8.png "在这里输入图片标题")

##### 发布帖子页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnZwpn.png "在这里输入图片标题")

##### 操作分类页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnZfpR.png "在这里输入图片标题")

##### 操作标签页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnZbAe.png "在这里输入图片标题")

##### 新建标签页面

![输入图片说明](https://z3.ax1x.com/2021/04/03/cnZvct.png "在这里输入图片标题")


